db.beans.insertMany([
    {
    "name": "Cappuchino",
    "size": "Tall",
    "price": 250,
    "inStock": true
    },
    {
        "name": "Latte",
        "size": "Venti",
        "price": 150,
        "inStock": true
    },
    {
        "name": "Americano",
        "size": "Venti",
        "price": 150,
        "inStock": false
    },
    {
        "name": "Espresso",
        "size": "Tall",
        "price": 220,
        "inStock": true
    }
])

db.beans.find({ "size": "Venti"});

db.beans.find({ "inStock": true});

db.beans.find({ $and: [{ "inStock": true}, {"price": {$gt: 150}}]});

db.beans.find({ $and: [{ "inStock": true}, {"price": {$lt: 200}}]});