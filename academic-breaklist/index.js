let http = require("http");

let mockDataBase = [
  {
    productName: "Mug",
    stock: 50,
    isAvailable: true
  },
  {
    productName: "Pencil",
    stock: 35,
    isAvailable: true
  }
];
let port = 4000;

let app = http.createServer(function (request, response) {
  if (request.url == "/items" && request.method == "GET") {
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify(mockDataBase));
    response.end();
  }


  if (request.url == "/items" && request.method == "POST") {

    let requestBody = "";

    request.on("data", function (data) {
 
      requestBody += data;
    });

    request.on("end", function () {
      console.log(typeof requestBody);

      requestBody = JSON.parse(requestBody);

      let newItems = {
        productName: "Plate",
        stock: 35,
        isAvailable: true
      };

      mockDataBase.push(newItems);
      console.log(mockDataBase);

      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(JSON.stringify(newItems));
      response.end();
    });
  }
});

app.listen(port, () => console.log("Server running at localhost:4000"));
