// Funtion
/* 
    Syntax:
        function functionName(){

        };
*/

function printName() {
    console.log("My name is Marwin")
};

printName();

// Funtion declaration vs Expressions

function declaredFunction(){
    console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function expression
let variableFunction = function(){
    console.log("Hello again!");
};

variableFunction();

declaredFunction = function(){
    console.log("Updated declaredFunction")
};

declaredFunction();

const constantFunc = function(){
    console.log ("Initialized with const!")
};

// constantFunc();

// constantFunc = function(){
//     console.log("Cannot be reassigned")

// }

// constantFunc();

// Funtion scoping

/*
    Scope is the accessibility (visibility) of variables

    JS Variables has 3 types of scope:
    1. local/block scope
    2. global scope
    3. function scope
*/

{
    let localVar = "Alonzo Mattheo";
console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function showNames(){
    // Function scoped variables
    var functionVar = "joe";
    const functionConst = "John";
    let functionLet = "jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();

// Nested Functions

function myNewFunction(){
    let name ="jane";

    function nestedFunction(){
        let nestedName = "john";
        console.log(name);
    }

    nestedFunction();
}

myNewFunction();

// Function and Global scoped variables

let globalName = "Joy";

function myNewFunction2(){
    let nameInside = "Kenzo"

    console.log(globalName)
}

myNewFunction2();

// Using alert()

alert("Hello World!")

function showSampleAlert(){
    alert("Hello, User!");

}
showSampleAlert();

// Using prompt()

let samplePrompt = prompt("Enter your name:")

console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
    let firstName = prompt("Enter you first name: ");
    let lastName = prompt("Enter your last name: ");

    console.log("Hello, " + firstName + " " + lastName + "!");
    console.log("Welcome to my page!")
}

printWelcomeMessage();

/*
Funtion naming conventions
    - Funtion names should be definitive of the task it will perform
    - Avoid generic names to avoid confusion within the code
    - Avoid pointless and inappropriate function names
    - Name your functions following camel casing
    - Do not use JS reserved keywords
*/

