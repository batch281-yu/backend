console.log("Hello")
function login(username, password, role) {
    // if statement to check for empty strings or undefined values
    if (
      username == "" ||
      username == undefined ||
      password == "" ||
      password == undefined ||
      role == "" ||
      role == undefined
    ) {
      console.log("Inputs must not be empty.");
    } else {
      // switch statement to check user role
      switch (role) {
        case "admin":
          console.log(`Welcome back to the class portal, admin!`);
          break;
        case "teacher":
          console.log(`Welcome back to the class portal, teacher!`);
          break;
        case "student":
          console.log(`Welcome back to the class portal, student!`);
          break;
        default:
          console.log("Role out of range.");
      }
    }
  }
  
  // Input Checking
  login();
  login("", "password", "admin");
  login("adminUser", "", "admin");
  login("adminUser", "password", "");
  
  login("adminUser", "password", "admin");
  login("teacherUser", "password", "teacher");
  login("studentUser", "password", "student");
  login("studentUser", "password", "carpenter");
  
  /*
      2. Create a function called checkAverage able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
          -add parameters appropriate to describe the arguments.
          -create a new function scoped variable called average.
          -calculate the average of the 4 number inputs and store it in the variable average.
          -research the use of Math.round() and round off the value of the average variable.
              -update the average variable with the use of Math.round()
              -Do not use Math.floor()
              -console.log() the average variable to check if it is rounding off first.
          -add an if statement to check if the value of average is less than or equal to 74.
              -if it is, return the following message:
              "Hello, student, your average is <show average>. The letter equivalent is F"
          -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
              -if it is, return the following message:
              "Hello, student, your average is <show average>. The letter equivalent is D"
          -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
              -if it is, return the following message:
              "Hello, student, your average is <show average>. The letter equivalent is C"
          -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
              -if it is, return the following message:
              "Hello, student, your average is <show average>. The letter equivalent is B"
          -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
              -if it is, return the following message:
              "Hello, student, your average is <show average>. The letter equivalent is A"
          -add an else if statement to check if the value of average is greater than 96.
              -if it is, return the following message:
              "Hello, student, your average is <show average>. The letter equivalent is A+"
  
          Invoke and add a number as argument using the browser console.
  
      Note: strictly follow the instructed function names.
  */
  
  function checkAverage(num1, num2, num3, num4) {
    let average = (num1 + num2 + num3 + num4) / 4; // calculate average
    average = Math.round(average); // update average variable with rounded value of itself
    console.log(average); // log value of average
    // If statement to check value of average and print the appropriate letter grade
    if (average <= 74) {
      console.log(
        `Hello, student, your average is ${average}. The letter equivalent is F`
      );
    } else if (average >= 75 && average <= 79) {
      console.log(
        `Hello, student, your average is ${average}. The letter equivalent is D`
      );
    } else if (average >= 80 && average <= 84) {
      console.log(
        `Hello, student, your average is ${average}. The letter equivalent is C`
      );
    } else if (average >= 85 && average <= 89) {
      console.log(
        `Hello, student, your average is ${average}. The letter equivalent is B`
      );
    } else if (average >= 90 && average <= 94) {
      console.log(
        `Hello, student, your average is ${average}. The letter equivalent is A`
      );
    } else if (average >= 96) {
      console.log(
        `Hello, student, your average is ${average}. The letter equivalent is A+`
      );
    }
  }
  
  checkAverage(70, 70, 72, 71);
  checkAverage(76, 76, 77, 79);
  checkAverage(81, 83, 84, 85);
  checkAverage(87, 88, 88, 89);
  checkAverage(91, 90, 92, 90);
  checkAverage(96, 95, 97, 97);
  
  //Do not modify
  //For exporting to test.js
  //Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
  try {
    module.exports = {
      login: typeof login !== "undefined" ? login : null,
      checkAverage: typeof checkAverage !== "undefined" ? checkAverage : null,
    };
  } catch (err) {}
  

