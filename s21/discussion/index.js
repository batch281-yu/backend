// Array Traversal

let grades =[98.5,94.3,89.2,90.1];
let computer = ['Acer','Asus','Lenovo','Neo','Redfox','Gateway','Toshiba','Fujitsu'];

let mixedArr = [12, 'Asus',null,undefined]; // This is not recommended

let tasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

// Creating an array with values from variables

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Mumbai";

let cities = [city1,city2,city3];

console.log(tasks);
console.log(cities);

// Array length property
console.log("Array length");

console.log(tasks.length);
console.log(cities.length);

let fullName ="Aizaac Estiva";
console.log(fullName.length);

tasks.length =tasks.length -1;
console.log(tasks.length);
console.log(tasks)

cities.length--;
console.log(cities);

fullName.length = fullName.length -1;
console.log(fullName.length)

// Adding a number to lengthen the size of the array
let theBeatles = ["John", "Paul","Ringo","George"];
theBeatles.length++;
console.log(theBeatles)

// Accesing elements of an array

console.log(grades[0]);
console.log(computer[3]);
console.log(grades[20])

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

console.log("Accesing arays using variables")
let currentLaker = lakersLegends[2];
console.log(currentLaker)

// Reassigning values in array
console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("After reassignment");
console.log(lakersLegends);

// Accesing the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length -1;

console.log(bullsLegends[lastElementIndex]);

// Directly access the expression

console.log(bullsLegends[bullsLegends.length - 1]);

// Adding items into the array
console.log("Adding item in aray")
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

let newVar = prompt("Enter a name:");
newArr[1] = newVar;
console.log(newArr)

// Looping through array
for(let index = 0; index < computer.length; index++){
console.log(computer[index]);
}

// Checks the element if its divisible by 5

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index <numArr.length; index++){
    if(numArr[index] % 5 === 0){
        console.log(numArr[index] + " is divisible by 5");
    }
    else{
        console.log(numArr[index] + " is not divisible by 5");
    }
}

// Multidimensional Array
/*
    2 x 2 Two dimensional array
    let twoDim = [[elem1,elem2],[elem3,elem4]]
                    0       1       0      1
                        0           1
    twoDim[0][0];
*/
let twoDim = [['Kenzo','Alonzo'],['Bella','Aizaac']];

console.log(twoDim[0][1]);
console.log(twoDim[1][0]);
console.log(twoDim[1][1]);

let twoDimLen = twoDim.length;
console.log(twoDimLen)