// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names.
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
let trainer = {
  // Initialize/add the given object properties and methods

  name: "Pedon Yu",
  age: 25,
  pokemon: ["Pikachu", "Bulbasaur", "Squirttle", "Charizard"],
  friends: {
    hoenn: ["Jedon", "Ned"],
    kanto: ["Zandro", "Jonathan"],
  },
  talk: function () {
    console.log("Pikachu! I choose you!");
  },
};
console.log(trainer);

// Access object using dot notation
console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
// Access object using square bracket notation
console.log(trainer["pokemon"]);
console.log("Result of talk method");
// Access talk trainer
trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = 1 * level;

  // Tackle method that subtracts health property of the target pokemon object with the attack property of the object that used the tackle method
  this.tackle = function (attackedPokemon) {
    console.log(this.name + " tackled " + attackedPokemon.name);
    attackedPokemon.health = attackedPokemon.health - 10;
    console.log(
      attackedPokemon.name +
        "'s" +
        " health is now reduced to " +
        attackedPokemon.health
    );

    //Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
    if (attackedPokemon.health <= 0) {
      this.faint(attackedPokemon);
    }
  }; //Faint Method message
  this.faint = function (pokemon) {
    console.log(pokemon.name + " has fainted.");
  };
}

// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
// Create/instantiate a new pokemon
let bulbasaur = new Pokemon("Bulbasaur", 2);
console.log(bulbasaur);
// Create/instantiate a new pokemon
let mew = new Pokemon("Mew", 15);
console.log(mew);
// Create/instantiate a new pokemon
let jigglyPuff = new Pokemon("Jiggly Puff", 18);
console.log(jigglyPuff);

// Invoke the tackle method and target a different object
pikachu.tackle(mew);
// log attackedPokemon current health
console.log(mew);

// Invoke the tackle method and target a different object
jigglyPuff.tackle(bulbasaur);
// log attackedPokemon current health
console.log(bulbasaur);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    trainer: typeof trainer !== "undefined" ? trainer : null,
    Pokemon: typeof Pokemon !== "undefined" ? Pokemon : null,
  };
} catch (err) {}
