// // Exponent Operator (**)

// const firstNum = 8 ** 2;
// console.log(firstNum);

// const secondNum = Math.pow(8, 2);
// console.log(secondNum);

// // Template Literals

// let name = "John";

// // pre=template literal string
// let message = "Hello" + name + "! Welcome to programming!";
// console.log("Message without template lilterals: " + message);

// // String using template literals (``)
// message = `Hello ${name}! Welcome to programming!`;
// console.log(`Message with template literals: ${message}`);

// // Multi-line using template literals
// const anotherMessage = `
// ${name} attended a math competition.
// He won it by solving the problem 8 ** 2 with the solution of ${firstNum};
// `;
// console.log(anotherMessage);

// const interestRate = 0.1;
// const principal = 1000;

// console.log(
//   `The interest on your savings account is: ${principal * interestRate}`
// );

// // Array destructuring
// /*
//     -allows us to unpack elements in arrays into distinct variables
//     -allows us to name array elements with variables instead of using index numbers
//     Syntax:
//         let/const [variableName, variableName,variableName] = array;
// */

// const fullName = ["Juan", "Castro", "Dela Cruz"];

// // Pre-array destructuring
// console.log(fullName[0]);
// console.log(fullName[1]);
// console.log(fullName[2]);

// console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);

// // Array destructuring
// const [firstName, middleName, lastName] = fullName;

// console.log(
//   `Hello ${firstName} ${middleName} ${lastName}! Its nice to meet you!`
// );

// // Object Destructuring
// /*
//     -allows us to unpack properties of objects into distinct variables
//     Syntax:
//         let/const {propertyName, propertyName, propertyName} = object
// */

// const person = {
//   givenName: "Jane",
//   maidenName: "Castro",
//   familyName: "Dela Cruz"
// };

// // Pre-object destructuring
// console.log(person.givenName);
// console.log(person.maidenName);
// console.log(person.familyName);

// console.log(
//   `Hello ${person.givenName} ${person.maidenName} ${person.familyName}! Its good to see you!`
// );

// // Object Destructuring
// const { givenName, maidenName, familyName } = person;
// console.log(givenName);
// console.log(maidenName);
// console.log(familyName);

// console.log(
//   `Hello ${givenName} ${maidenName} ${familyName}! Its good to see you!`
// );

// // Arrow functions
// /*
//     -Compact alternative syntax to traditional functions
//     Syntax:
//         const/let variableName = () => {
//             console.log();
//         }
// */

// const hello = () => {
//   console.log("Hello, world!");
// };

// // Pre-arrow function and template literals
// /*
//     function printFullName (firstName, middleInitial, lastName){
//         console.log(firstName + ' ' + middleInitial + '. ' + lastName)
//         printFullName("John", "D", "Smith")
//     }
// */
// // Implementation of Arrow Function

// const printFullName = (firstName, middleInitial, lastName) => {
//   console.log(`${firstName} ${middleInitial}. ${lastName}`);
// };

// printFullName("John", "D", "Smith");

// // Arrow functions with loops
// // Pre-arrow function

// const students = ["John", "Jane", "Joy"];

// console.log(`Using traditional function`);

// students.forEach(function (student) {
//   console.log(`${student} is a student.`);
// });

// // Arrow function in loops
// console.log(`Using arrow function`);
// students.forEach((student) => {
//   console.log(`${student} is a student`);
// });

// // IMplicit return statement
// // Pre-arrow function
// /*
//     const add = (x, y) => {
//         return x + y;
//     }

//     let total = add(1, 2);
//     console.log(total;)
// */
// // Implicit Return
// const add = (x, y) => x + y;

// let total = add(1, 2);
// console.log(total);

// // Default function argument value

// const greet = (name = "User") => {
//   return `Good morning, ${name}!`;
// };

// console.log(greet());
// // console.log(greet('Ray'));

// // Class based object blueprints

// // Creating a class

// class Car {
//   constructor(brand, name, year) {
//     this.brand = brand;
//     this.name = name;
//     this.year = year;
//   }
// }

// // Instantiating an object

// const myCar = new Car();

// console.log(myCar);

// // Values of properties assigned after instantiation of an object

// myCar.brand = "Ford";
// myCar.name = "Ranger Raptor";
// myCar.year = "2021";

// console.log(myCar);

// // Instantiate a new object from the car class with initialized values
// const myNewCar = new Car("Toyota", "Vios", 2021);

// console.log(myNewCar);

const foo = 1;
foo = 2;
