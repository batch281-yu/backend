// JSON object
/*
    -JSON stands for JavaScript Object Notation
    Syntax:
        {
            "propertyA": "valueA",
            "propertyB": "valueB"
        }
*/

// JSON as Object
// {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"
// }

// JSON Arrays
// "cities": [
//     {"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
//     {"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
//     {"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}

// ]
// Mini Activity -Create a JSON Array that will hold three breeds of dogs with properties:

// "dogs": [
//     {"name": "Momo", "age": "12", "breed": "Toy poodle"},
//     {"name": "Sky", "age": "2", "breed": "Husky"},
//     {"name": "Max", "age": "3", "breed": "Labrador"}
// ]

// JSON Methods

// Convert Data into Stringified JSON

let batchesArr = [{ batchName: "Batch X" }, { batchName: "Batch Y" }];

// the "stringify" method is used to convert JavaScript objects into string
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
  name: "John",
  age: 31,
  address: {
    city: "Manila",
    country: "Philippines",
  },
});
console.log(data);

// Using Stringify Method with variables
// User details
// let firstName = prompt('What is your first name?')
// let lastName = prompt('What is your lastn name?')
// let age = prompt('What is your age?')
// let address = {
//     city: prompt('Which city do you live in'),
//     country: prompt('Which country does your city address belong to?')
// };

// let otherData = JSON.stringify({
//     firstName: firstName,
//     lastName: lastName,
//     age: age,
//     address: address
// })
// console.log(otherData)

// // Mini Activity -

// let carBrand = prompt('What is the brand of your car?')
// let carType= prompt('What type is your car?')
// let carYear = prompt('What year is your car?')
// ;

// let carDetails = JSON.stringify({
//     carBrand: carBrand,
//     carType: carType,
//     carYear: carYear
// })
// console.log(carDetails)

// Converting Stringify JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch " }]`;

console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));
