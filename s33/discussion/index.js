// console.log("Hello World")

// [Section] Javascript Synchronous vs Asynchronus
// JavaScript is by default synchronus meaning that only one statement is executed at a time

// This can be proven when a statement has an error, javscript will not proceed with the next statement
console.log("Hello")
// conslone.log("hello again")
console.log("Goodbye")

// When certain statements takes a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this result in code "blocking"
// We might not notice it due to the fast processing power of our device
// This is the reason why some websites dont instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed

// Asynchronus means that we can proceed to execute other statements, while time consuming code is running in the background

// Create a simple fetch request

// [Section] Getting all posts

// The fetch API allows you to asynchronusly request for a resource(data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Check the status of the request

// Retrieve all posts following the REST API(retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
// The "fetch" method will return a "promise" that resolves to a "Response" object
// The "then" method captures the "Response" object and returns another "promise" which will eventually be "Resolved" or "Rejected"
.then(response => console.log(response.status))

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our collection
.then((response) => response.json())
// Print the converted JSON value from the "fetch" request
// Using multiple "then" methhods creates a promise chain
.then((json) => console.log(json))

// Demonstrate using the "async" and "await" keywords

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indacate which portions of code should be waited for
// Creates asynchronous function
async function fetchData(){
    
    // waits for the "fetch" method to complete then stores the value in the "result" variable
    let result = await fetch('https://jsonplaceholder.typicode.com/posts')
    // Result returned by fetch returns a promise
    console.log(result);
    // The returned "Response" is an object
    console.log(typeof result);
    // We cannot access the content of the "Response" by directly accessing its body's property
    console.log(result.body);

    // Converts data from the "Response" object as JSON
    let json = await result.json();
    // Print out the content of the "Response" object
    console.log(json)
};

fetchData()

// Process a GET request using postman


// [Section] Getting a specific post

// Retrieves a specific post following the REST API (retrieve, /posts/:id,GET)

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json))

// [Section] Creating a new post

// Creates a new post following the REST API(create, /posts/:id, POST)

fetch('https://jsonplaceholder.typicode.com/posts', {
    // Sets the method of the "Request" object to "POST" following the REST API
    // Default method is GET
    method:'POST',
    // Sets the method of the "Request" object to "POST" following the REST API
    // Specified that the content will be in a JSON structure

    headers: {
        'Content-type': 'application/json', 
    },
    // Sets the content/body data of the "Request" object to be sent to the backend
    // JSON.stringify converts the object data into a stringified JSON
    body: JSON.stringify({
        title: 'New post',
        body: 'Hello World!',
        userID: 1
    }),
})
.then((response)=> response.json())
.then((json)=> console.log(json))

// [Section] Updating a post
// Updates a specific post followong the REST API

fetch('https://jsonplaceholder.typicode.com/posts/1', {  
    method:'PUT',   
    headers: {
        'Content-type': 'application/json', 
    },   
    body: JSON.stringify({
        title: 'Updated Post',
        body: 'Hello Again',
        userID: 1
    }),
})
.then((response)=> response.json())
.then((json)=> console.log(json))

// [Section] Updating a post using PATCH

// Updates a specific post following the REST API(update, /posts:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PUT is used to update the whole object
// PATCH is used to update a single/several properties
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PATCH',
    headers: {
        'Content-type': 'application/json',
    },
    body:  JSON.stringify({
        title: 'Corrected post',
    })
})
.then((response)=> response.json())
.then((json)=> console.log(json))

// [Section] Deleting post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'DELETE'
})

// [Section] Filtering posts

// The data can be filtered by sending the userId along with the URL
// Information sent via the url can be done by adding the question mark symbol(?)
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response)=> response.json())
.them((json)=> console.log(json))

// [Section] Retreiving nested/related comments to post

// Retrieving comments for a specific post followong the REST API(retrieve, /posts/:id/comments)
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=> response.json())
.them((json)=> console.log(json))
