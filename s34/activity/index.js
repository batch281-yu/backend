const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));


// /home route
app.get("/home", (req, res) => {
    res.send("Welcome to the home page")
})
let users = [
    {
        "username": "johndoe",
        "password": "johndoe1234"
    }
]
// /users route
app.get("/users", (req, res) => {
    res.send(users)
})

// /delete-user route
app.delete("/delete-user", (req, res) => {
    const username = req.body.username;
    let message;
  
    if (users.length > 0) {
      for (let i = 0; i < users.length; i++) {
        if (users[i].username === username) {
          users.splice(i, 1);
          message = `User ${username} has been deleted.`;
          break;
        }
      }
  
      if (message === undefined) {
        message = `User does not exist.`;
      }
    } else {
      message = `No users found.`;
    }
  
    res.send(message);
  });

if(require.main === module){
    app.listen(port, () => console.log(`Server is running at ${port}`))
}

module.exports = app;