const express = require ("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://yumarwin219:TD01JGU20jResQDI@wdc028-course-booking.rjg3nnq.mongodb.net/b281_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }

);

// Set notification for connection success or failure
let db = mongoose.connection;

// If a connection error occurred, output in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Mogoose Schema
const taskSchema = new mongoose.Schema({
    name : String,
    status : {
        type : String,
        default : "pending"
    }
})

// Models [Task models]
const Task = mongoose.model("Task", taskSchema);

// Allows the app to read json data
app.use(express.json());
// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}))

// Creation of todo list routes

// Creating a new task
app.post("/tasks", (req, res) => {
    // If a document was found and documents name matches the information from the client
    Task.findOne({name : req.body.name}).then((result, err) => {
        if(result != null && result.name == req.body.name){
        // Return a message to the client/Postman
        return res.send("Duplicate task found");
        }
        // If no document was found
        else{
            // Create a new task and save it to the database
            let newTask = new Task({
                name : req.body.name
            });

            newTask.save().then((savedTask, saveErr) => {
                // If there are errors in saving
                if(saveErr){
                    return console.error(saveErr);
                }
                // No error found while creating the document
                else{
                    return res.status(201).send("New task created");
                }
            })
        }
    }) 
})

// Get all the tasks
app.get("/tasks", (req, res) => {
    Task.find({}).then((result, err) => {
        // If an error occured
        if(err){
            // Will print any errors found in the console
            return console.log(err);
        }
        // If no errors are foud
        else{
            return res.status(200).json({
                data: result
            })
        }
    })
})

//! Activity Part

const userSchema = new mongoose.Schema({
    username: String,
    password: String
  });
  const User = mongoose.model("User", userSchema);
  app.post("/signup", (req, res) => {
    User.findOne({ username: req.body.username }).then((result, err) => {
      if (result !== null && result.username === req.body.username) {
        return res.send("Duplicate user found");
      } else {
        let newUser = new User({
          username: req.body.username,
          password: req.body.password
        });
        newUser.save().then((savedUser, saveErr) => {
          if (saveErr) {
            return console.log(saveErr);
          } else {
            return res.status(201).send("New user registered");
          }
        });
      }
    });
  });
  app.get("/users", (req, res) => {
    User.find({}).then((result, err) => {
      if (err) {
        return console.log(err);
      } else {
        return res.status(200).json({
          data: result
        });
      }
    });
  });



// Listen to port
app.listen(port, () => console.log(`Server running at port ${port}`));