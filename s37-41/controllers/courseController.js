const Course = require("../models/Course");

// ACTIVITY
// module.exports.addCourse = (reqBody) => {

//     // Creates a variable newCourse and instantiates a new 'Course object using the mongoose model
//     // Uses the informatio from the request body to provide all the necessary information
//     let newCourse = new Course({
//         name : reqBody.name,
//         description : reqBody.description,
//         price : reqBody.price
//     });

//     return newCourse.save().then((course, error) => {
//         if(error){
//             return false;

//         } else {
//             return true;
//         };
//     })
// };


module.exports.addCourse = (data) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};

// Retreive all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    });
};

// Retreive all active courses
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    });
};

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
}

// Updating a course
module.exports.updateCourse = (reqParams, reqBody) => {

    // Specify the fields/properties of the document to be updated
    let updateCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };

    return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) =>{
        // Course not updated
        if(error){
            return false
        // Course updated succesfully
        }else{
            return true
        }
    })
}

// activity
module.exports.archiveCourse = (reqParams, reqBody) => {
    let archiveCourse = {
        isActive: reqBody.isActive
    }
    return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error)=> {
        if(error){
            return false
        }else {
            return true
        }
    })
}