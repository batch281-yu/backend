const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express()

// Connect to MongoDB
mongoose.connect("mongodb+srv://yumarwin219:TD01JGU20jResQDI@wdc028-course-booking.rjg3nnq.mongodb.net/s37-41API?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology: true
});
mongoose.connection.on('error', console.error.bind(console,"MongoDB connection Error"));

mongoose.connection.once('open', ()=>console.log('Now connected to MonggoDB Atlas!'));

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000,()=>{
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});