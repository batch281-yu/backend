const express = require("express");
const router = express.Router()
const courseController = require("../controllers/courseController");
const auth = require("../auth");


// ACTIVITY
// Route for creating a course
// router.post("/", auth.verify, (req, res) => {
//     let userData =auth.decode(req.headers.authorization);

//     if (userData.isAdmin){
//         courseController.addCourse(req.body).then((result)=>{
//             res.send(result);
//         });
//     } else {
//         res.send({ auth: "failed"})
//     }
//     });

router.post("/", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    if(data.isAdmin == true){
        courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

    }else {
        res.send(false)
    }
	
});

// Route for retreiving all the courses
router.get("/all", (req, res)=>{
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// Route for retreiving all the active courses
router.get("/", (req, res)=>{
    courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving a specific course
// Creating a course using a  :parameterName crates a dynamic route, meaning the url changes depending on the information provided
router.get("/:courseId", (req, res)=>{
    console.log(req.params.courseId);

    // Since the course id will not be sent via the URL, we cannot retrieve it from the request body
    // we can however retrieve the course id by accessing the requests params property which contains all the parameters provided via the URL
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// ACTIVITY
router.patch("/:courseId", auth.verify, (req,res) => {
    const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    if(data.isAdmin == true){
        courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
    }else {
        res.send(false)
    }
    
})
// Allows us to export the router object that will be accessed in our index.js file
module.exports = router;