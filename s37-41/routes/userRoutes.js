const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the email already exist in the database
// Invokes the checkEmailExist function from the controller file
router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route fot authetication
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// ACTIVITY
// router.post("/details", (req, res)=>{
//     userController.getProfile(req.body).then((resultFromController)=>{
//         res.send(resultFromController)
//     })
// })

// The auth.verify acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {
  // Uses decode method defined in the auth.js file to retrive user information from the token passing the token from the request header
  const userData = auth.decode(req.headers.authorization);

  // Provides the user's ID for the getProfile controller method
  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
  let data = {
    userData: auth.decode(req.headers.authorization),
    courseId: req.body.courseId
  };

  userController
    .enroll(data)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
