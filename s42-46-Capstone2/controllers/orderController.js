const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");

// module.exports.addOrder = async (req, res) => {
//   //console.log(req.body);

//   let data = {
//     userId: req.body.userId,
//     productId: req.body.productId,
//     quantity: req.body.quantity,
//     isAdmin: `${auth}`
//   };

//   let product = await Product.findById(req.body.productId).then(
//     (foundProduct) => {
//       console.log(foundProduct);
//       if (data.isAdmin) {
//         return res.send({ message: "Admins can't place order" });
//       }
//       if (!data.isActive) {
//         return res.send({ message: "Product not available." });
//       }

//       let newOrder = new Order({
//         totalAmount: data.totalAmount,
//         quantity: data.quantity,
//         products: [
//           {
//             productId: data.productId,
//             quantity: data.quantity,
//             price: data.price
//           }
//         ],
//         userId: data.userId
//       });

//       newOrder
//         .save()
//         .then((result) => res.send({ message: "Order Successful" }))
//         .catch((error) => res.send(error));
//     }
//   );
// };

// module.exports.addOrder = async (req, res) => {
//   let data = {
//     isAdmin: auth,
//     userId: req.body.userId,
//     productId: req.body.productId,
//     quantity: req.body.quantity
//   };

//   let product = await Product.findById(data.productId)
//     .then((product) => product)
//     .catch((error) => error);

//   if (!product.isActive) {
//     return res.send("Sorry. Product selected is out of stock");
//   }

//   if (data.isAdmin === false) {
//     let newOrder = new Order({
//       userId: data.userId,
//       products: {
//         productId: data.productId,
//         quantity: data.quantity
//       },
//       totalAmount: data.quantity * product.price
//     });

//     return newOrder
//       .save()
//       .then(() => res.send("Thank you for your purchase!"))
//       .catch((error) => {
//         console.error("Error. Order cannot be added:", error);
//         res.send(false);
//       });
//   } else {
//     res.status(403).send("Hindi pwede, boss ka e!");
//   }
// };

// module.exports.addOrder = (req, res) => {
//   //console.log(req.body);
//   let data = {
//     userId: req.body.userId,
//     productId: req.body.productId,
//     quantity: req.body.quantity
//   };
//   let product = Product.findById(req.body.productId).then((foundProduct) => {
//     console.log(foundProduct);
//     if (newOrder.isAdmin) {
//       return res.send({ message: "Admins can't place order" });
//     }
//     let newOrder = new Order({
//       userId: data.userId,
//       products: {
//         productId: data.productId,
//         quantity: data.quantity
//       },
//       totalAmount: data.quantity * product.price
//     });

//     newOrder
//       .save()
//       .then((result) => res.send({ message: "Order Successful" }))
//       .catch((error) => res.send(error));
//   });
// };

module.exports.addOrder = (req, res) => {
  //console.log(req.body);

  let newOrder = new Order({
    totalAmount: req.body.totalAmount,
    quantity: req.body.quantity,
    products: [
      {
        productId: req.body.productId,
        quantity: req.body.quantity,
        price: req.body.price
      }
    ],
    userId: req.body.userId
  });

  Product.findById(req.body.productId).then((foundProduct) => {
    console.log(foundProduct);
    if ((isAdmin = false)) {
      return res.send({ message: "Admins can't place order" });
    }
    newOrder
      .save()
      .then((result) => res.send({ message: "Order Successful" }))
      .catch((error) => res.send(error));
  });
};
