const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cors = require("cors");
const port = process.env.PORT || 4000;

mongoose.connect(
  "mongodb+srv://yumarwin219:TD01JGU20jResQDI@wdc028-course-booking.rjg3nnq.mongodb.net/Capstone3-backend?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB Connection Error."));

db.once("open", () => console.log("Connected to MongoDB."));

app.use(express.json());
app.use(cors());

const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use("/products", productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use("/orders", orderRoutes);

app.listen(port, () => console.log("ExpressJS API running at localost:4000"));
