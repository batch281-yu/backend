const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order")



module.exports.addOrder = (req, res)=>{
	//console.log(req.body);

	let newOrder = new Order({
		userId 	   : req.body.userId,
		products   : [
            {
                productId: req.body.productId, 
                quantity: req.body.quantity
            }
        ],
        totalAmount: req.body.totalAmount

	})

	return Product.findById(req.body.productId).then(foundProduct => { 
		if(User.isAdmin === true){
				return res.send({auth: "Admins can't place order"})
			}
			
			if(foundProduct === null){
				return res.send({auth: "Product not available."})
			}

			newOrder.save()
				    .then(result => res.send({auth: "Order Successful"}))
				    .catch(error => res.send(error))
		})
}
