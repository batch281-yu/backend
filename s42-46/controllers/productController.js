const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order")
const bcrypt =require("bcrypt");
const auth = require("../auth");



// todo  CREATE PRODUCT
module.exports.addProduct = (data) => {

    let newProduct = new Product({
        name : data.product.name,
        description : data.product.description,
        price : data.product.price
    });

    return newProduct.save().then((product, error) => {
        if(error){
            return false;
        } else {
            return true;
        };
    })
};

// todo RETRIEVE ALL PRODUCTS
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    })
};

// todo RETRIEVE ALL ACTIVE PRODUCTS
module.exports.getAllActive = () => {
    return Product.find({isActive:  true}).then(result => {
        return result;
    });
};

// todo RETRIEVE SINGLE PRODUCT
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
};

// todo UPDATE PRODUCT (ADMIN ONLY)
module.exports.updateProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error)=> {
        if(error){
            return false
        } else {
            return true
        }
    })
};

// todo ARCHIVE A PRODUCT (ADMIN ONLY)
module.exports.archiveProduct = (reqParams, reqBody) => {
    let archivedProduct = {
        isActive: reqBody.isActive
    }
    return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error)=> {
        if(error){
            return false;
        } else {
            return true;
        }
    })
};

// todo ACTIVATE A PRODUCT (ADMIN ONLY)
module.exports.activateProduct = (reqParams, reqBody) => {
    let activatedProduct = {
        isActive: reqBody.isActive
    }
    return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((product, error)=> {
        if(error){
            return false;
        } else {
            return true;
        }
    })
};