const User = require("../models/User");
const Course = require("../models/Product");
const Order = require("../models/Order")
const bcrypt =require("bcrypt");
const auth = require("../auth");

// ! CHECK EMAIL
module.exports.checkEmailExists = (reqBody)=>{
    return User.find({email : reqBody.email}).then(result => {
        if(result.length > 0){
            return true;
        } else {
            return false;
        };
    });
};

// todo USER REGISTRATION
module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password,10)
    })

    return newUser.save().then((user, error) => {
        if(error){
            return false
        } else {
            return true
        }
    })
};

module.exports.loginUser = (reqBody) => {

    return User.findOne({email : reqBody.email}).then(result => {
        if(result == null){
            return false;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return { access: auth.createAccessToken(result)}
            } else {
                return false;
            }
        }
    })
}


