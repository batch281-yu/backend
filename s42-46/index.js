const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express()




// Connect to MongoDb
mongoose.connect("mongodb+srv://yumarwin219:TD01JGU20jResQDI@wdc028-course-booking.rjg3nnq.mongodb.net/Capstone2API?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology: true
});
mongoose.connection.on('error', console.error.bind(console,"MongoDB connection Error"));

mongoose.connection.once('open', ()=>console.log('Now connected to MonggoDB Atlas!'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


app.listen(process.env.PORT || 4000,()=>{
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});