const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");



router.post("/checkout", auth.verify,orderController.addOrder)



module.exports = router;