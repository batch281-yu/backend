const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// todo CREATE PRODUCT ROUTE
router.post("/", auth.verify, (req, res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin == true){
        productController.addProduct(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send(false)
    }
});

// todo RETRIEVE ALL PRODUCTS ROUTE
router.get("/all", (req, res) => {
    productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// todo RETRIEVE ALL ACTIVE PRODUCTS ROUTE
router.get("/", (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// todo RETRIEVE SINGLE PRODUCT
router.get("/:productId", (req,res) => {
    console.log(req.params.productId);

    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// todo UPDATE PRODUCT (ADMIN ONLY)
router.put("/:productId", auth.verify, (req, res) => {
    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
});

// todo ARCHIVE PRODUCT (ADMIN ONLY)
router.patch("/:productId", auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if(data.isAdmin == true){
        productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
    } else {
        res.send(false)
    }

});

// todo ACTIVATE A PRODUCT (ADMIN ONLY)
router.patch("/:productId", auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if(data.isAdmin == true){
        productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
    } else {
        res.send(false)
    }

});

module.exports = router;